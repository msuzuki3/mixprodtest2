data {
  int<lower=1> NT;
  int<lower=1> T;
  int<lower=1> N;
  int<lower=1> nmix;
  int<lower=1> nmix1;
  int<lower=1> nmix2;
  int id[NT];
  int time[NT];
  real m[NT];
  real k[NT];
  real l[NT];
  real y[NT];
  real sm[NT];
  real sl[NT];
  real w[N,nmix];
  int<lower=1,upper=nmix1> j1;
  int<lower=0, upper=1> timeVarying;

  real<lower=0.0, upper=2.0> bm[1 + timeVarying*(T-1)];
  real<lower=0.0, upper=2.0> bl[1 + timeVarying*(T-1)];
  cholesky_factor_cov[2] LEps;
  real at[T];
  real psi[nmix2];
  real<lower=0> sig_v[nmix2];
}

transformed data {

  matrix[N,nmix2] ll1;
  real eps1[NT];
  matrix[2,2] SigmaEps;

  SigmaEps = LEps * LEps';

  for (n in 1:NT) {
    int i;
    int t;
    vector[2] eps;
    vector[nmix2] v;
    i = id[n];
    t = time[n];

    eps[1] = log(bm[1 + timeVarying*(t-1)]) + 0.5*SigmaEps[1,1] - sm[n];
    eps[2] = sl[n] - sm[n] - log(bl[1 + timeVarying*(t-1)]) + log(bm[1 + timeVarying*(t-1)])
            + 0.5*SigmaEps[2,2];
    eps1[n] = eps[1];

    for (j in 1:nmix2) {

      if (n==1 || id[n]!=id[n-1]) { // new observation
        ll1[i,j] = 0;
      } else {
        if (time[n-1] != t-1) print("ERROR: data not sorted or panel unbalanced.");
      }

      v[j] = m[n] - l[n] - (at[t] + log(bm[1 + timeVarying*(t-1)]/bl[1 + timeVarying*(t-1)])
                     + 0.5*SigmaEps[2,2] + psi[j]);

      ll1[i,j] = ll1[i,j] + multi_normal_cholesky_lpdf(eps | rep_vector(0,2), LEps)
               + normal_lpdf(v[j] | 0, sig_v[j]);

    }
  }
}

parameters {
  real rho;
  real<lower=0.0, upper=2.0> bk;
  real<lower=0> sigEta;
  real b0[T];

  real rhoK[3];
  real<lower=0> sigK;
  cholesky_factor_cov[2] L0;
  real muk;
}

transformed parameters {
  matrix[N,nmix2] loglike_ij;
  matrix[N,nmix2] ll2;

  {
    real omega[T];
    for (j in 1:nmix2) {
      for (n in 1:NT) {

        int i;
        int t;
        vector[2] eps;

        i = id[n];
        t = time[n];
        if (n==1 || id[n]!=id[n-1]) { // new observation
          ll2[i,j] = 0;
        } else {
          if (time[n-1] != t-1) print("ERROR: data not sorted or panel unbalanced.");
        }

        //loglike_i[i] = loglike_i[i] + ll1[n];
        omega[t] = y[n] - bm[1 + timeVarying*(t-1)]*m[n] - bl[1 + timeVarying*(t-1)]*l[n]
                - bk*k[n] - eps1[n] - b0[t] - bl[1 + timeVarying*(T-1)]*psi[j];
        ll2[i,j] = ll2[i,j] + log(fabs(1 - bm[1 + timeVarying*(t-1)]
                - bl[1 + timeVarying*(t-1)]));
        if (n==1 || id[n] != id[n-1]) { // new observation
          vector[2] x;
          vector[2] mu;
          x[1] = omega[t];
          x[2] = k[n];
          mu[1] = 0;
          mu[2] = muk;
          ll2[i,j] = ll2[i,j] + multi_normal_cholesky_lpdf(x | mu, L0);
        } else {
           ll2[i,j] = ll2[i,j] + normal_lpdf(omega[t] | rho*omega[t-1], sigEta);
           ll2[i,j] = ll2[i,j] + normal_lpdf(k[n] |  rhoK[1] + rhoK[2]*k[n-1] +
                                       rhoK[3]*omega[t-1], sigK);
        }
      }
    }

    loglike_ij[:,:] = ll1[:,:] + ll2[:,:];

  }
}
model {
  for (n in 1:N) {
    for (j2 in 1:nmix2) {
      int j;
      j = j2 + (j1 -1)*nmix2;
      target += loglike_ij[n,j2]*w[n,j];
    }
  }
}

generated quantities {
  matrix[2,2] var0; // initial variance of omega,k
  var0 = L0 * L0';
}

