data {
  int<lower=1> NT;
  int<lower=1> T;
  int<lower=1> nmix;
  int<lower=1> nmix1;
  int<lower=1> nmix2;
  int<lower=1> N;
  int id[NT];
  int time[NT];
  real m[NT];
  real k[NT];
  real l[NT];
  real y[NT];
  real sm[NT];
  real sl[NT];
  real w1[N,nmix1];
  real w[N,nmix];
  int<lower=1,upper=nmix1> j;
  int<lower=0, upper=1> timeVarying;
}

transformed data {
  real sx[NT];
  for (n in 1:NT) {
    sx[n] = sl[n] - sm[n];
  }
}

parameters {
  real bm[1 + timeVarying*(T-1)]; // bm = log(bm) + 0.5*sig_eps^2
  real bl[1 + timeVarying*(T-1)]; // bl = - log(bl/bm) + 0.5*sig_zeta^2
  cholesky_factor_cov[2] LEps;
}

transformed parameters {
  real loglike_i[N];
  matrix[2,2] SigmaEps;
  // imposing zero covariance
  matrix[2,2] LEpsx;
  LEpsx = LEps;
  LEpsx[2,1] = 0;
  SigmaEps = LEps * LEps';
  // SigmaEps = LEpsx * LEpsx';

  {
    for (n in 1:NT) {
      int t;
      int i;
      vector[2] eps;

      t = time[n];
      i = id[n];
      if (n==1 || id[n]!=id[n-1]) { // new observation
        loglike_i[i] = 0;
      } else {
        if (time[n-1] != t-1) print("ERROR: data not sorted or panel unbalanced.");
      }

      eps[1] = bm[1 + timeVarying*(t-1)] - sm[n];
      eps[2] = bl[1 + timeVarying*(t-1)] + sx[n];

     loglike_i[i] = loglike_i[i] + multi_normal_cholesky_lpdf(eps | rep_vector(0,2),
                                                LEps);
      // loglike_i[i] = loglike_i[i] + multi_normal_cholesky_lpdf(eps | rep_vector(0,2),
      //                                            LEpsx);
    }
  }
}
model {
  for (n in 1:N) {
    target += loglike_i[n]*w1[n,j];
  }
}
