library(mixprodtest)
set.seed(1)

## Sample size
N <- 1000
T <- 25
J <- 2

## Parameters
prob <-c(0.5,0.5)
beta <- matrix(c(0.1,0.6,0.3,
                 0.4,0.1,0.5),
               nrow=J,ncol=3,byrow=TRUE)
sig.eps <- c(0.5,1)
sig.zeta <- c(1,1)
rho.ez <- c(0,0.5)

rho.omega <- c(0.6, 0.8)
sig.eta <- c(0.5,1)

## Capital process
# kt = mk + rhokk kt-1 + rhoko omt-1 + e
mk <- 0
rhokk <- 0.3
rhoko <- 0.3
sig.k <- 1


Sigma <- array(dim=c(J,2,2))
for (j in 1:J) {
  Sigma[j,,] <- matrix(c(sig.eps[j]^2,
                         sig.eps[j]*sig.zeta[j]*rho.ez[j],
                         sig.eps[j]*sig.zeta[j]*rho.ez[j],
                         sig.zeta[j]^2), nrow=2)
}

## Simulation
library(MASS)
library(tsDyn)
simulate <- function(N,T,J,prob,beta,Sigma) {
  id <- as.vector(outer(rep(1,T),1:N))
  t <- as.vector(outer(1:T,rep(1,N)))
  type <- sample.int(J,size=N,prob=prob,replace=TRUE)
  type.t <- as.vector(outer(rep(1,T),type))
  share <- matrix(nrow=N*T,ncol=2)
  material <- rep(NA,N*T)
  labor  <- rep(NA,N*T)
  capital<- rep(NA,N*T)
  output <- rep(NA,N*T)
  omega <- rep(NA,N*T)
  for (j in 1:J) {
    ok <- replicate(n=N,VAR.sim(B=matrix(c(0, rho.omega[j],0,
                                           mk,rhoko, rhokk),nrow=2,byrow=TRUE),
                                n=T, lag=1,innov=mvrnorm(n=T,mu=c(0,0),Sigma=diag(c(sig.eta[j],sig.k))),
                                include="const" ) )
    k <- as.vector(ok[,2,])
    om <- as.vector(ok[,1,])
    mu <- log(beta[j,1:2]) + 0.5*c(Sigma[j,1,1],
                                   Sigma[j,1,1] - Sigma[j,2,2])
    ez <- mvrnorm(N*T,mu=c(0,0),Sigma=drop(Sigma[j,,]))
    eps <- ez[,1]
    zeta <- ez[,2]
    s <- cbind(mu[1]-eps,mu[2]-eps+zeta)
    y <- (cbind(s,k) %*% beta[j,] + om + eps)/(1-beta[j,1]-beta[j,2])
    m <- s[,1]+y
    l <- s[,2]+y
    share[type.t==j,] <- s[type.t==j,]
    material[type.t==j] <- m[type.t==j]
    labor[type.t==j] <- l[type.t==j]
    capital[type.t==j] <- k[type.t==j]
    output[type.t==j] <- y[type.t==j]
    omega[type.t==j] <- om[type.t==j]
  }
  data.frame(id=id,t=t,type=type.t,
             share.m=share[,1],share.l=share[,2],material=material,
             labor=labor,capital=capital,output=output, omega=omega)

}

sdf <- simulate(N,T,J,prob,beta,Sigma)
summary(sdf)


# Testing MLE (EM algorithm)

var.names <- list(
  id="id",
  t="t",
  output="output",
  materials="material",
  labor="labor",
  capital="capital",
  material.share="share.m",
  labor.share="share.l")

opt <- mixprod.options() ## get a list of default options
opt$init.draws <- 1
opt$estimation.method <- "EM"
# opt$estimation.method <- "classification"
opt$classification.method <- "stan.em"
opt$second.stage.include.eps <- FALSE
opt$time.varying <- FALSE
opt$flex.labor <- TRUE

J <- 2 #  number of mixture components
estEM <- mixprod.estimate(data=sdf, J, var.names, options=opt)

# True parameter values

param_true <- rbind(prob, t(beta), sig.eps, sig.zeta, rho.ez,
                    rho.omega, sig.eta, rhokk, rhoko,sig.k, mk)
rownames(param_true) <- c("P(type)", "bm", "bl", "bk", "sig.eps",
                          "sig.zeta", "rho.ez", "rho.omega", "sig.eta",
                          "rhokk", "rhoko", "sig.k", "mk")
colnames(param_true) <- c("Type 1", "Type 2")
cat("\n")
cat("True parameter values\n")
print(param_true)

cat("\n")
cat("Estimated parameter values\n")
print(estEM$parameters)

param_true2 <- param_true[2:4,]
param_em <- rbind(estEM$parameters["bm",], estEM$parameters["bl",],
                  estEM$parameters["bk",])
param_compare <- cbind(param_true2, param_em)
cat("\n")
cat("True vs EM\n")
print(param_compare)

# library(rstan)
# step1 <- FALSE
# if (step1) {
#   model.stan <- "G:/Projects/Production_function/R/mixprodtest/exec/mle-step1.stan"
#   stan.mle1 <- stan_model(file=model.stan,
#                           model_name="MLE Step 1",
#                           verbose=FALSE)
#   initial.values <- list(bm=beta[,1], bl=beta[,2],
#                          prob=c(0.5,0.5),SigmaEps=Sigma)
#
#   stan.data <- list(NT=N*T, id=sdf$id, time=sdf$t, NTmiss=1,
#                     nmix=J,N=N,T=T,
#                     m=sdf$share.m, l=sdf$share.l, k=rep(0,N*T), y=rep(0,N*T))
#   opt <- optimizing(stan.mle1, data=stan.data, init=initial.values,
#                     algorithm="BFGS",verbose=TRUE,as_vector=FALSE,hessian=FALSE)
# }
#
# fileName <- "G:/Projects/Production_function/R/mixprodtest/exec/mle_joint.stan"
# txt <- readChar(fileName, file.info(fileName)$size)
# txt <- gsub("\n$","",txt)
# if (!exists("stan.mle") || txt != stan.mle@model_code[1]) {
#   stan.mle <- stan_model(file=fileName,
#                          model_name="MLE",
#                          verbose=TRUE,
#                          save_dso=TRUE)
# }
#
# initial.values <- list(rho=rho.omega,
#                        bm=beta[,1], bl=beta[,2],bk=beta[,3],
#                        prob=prob, sigEta=sig.eta,
#                        b0=matrix(0,nrow=J,ncol=T),
#                        rhoK=matrix(rep(c(mk,rhokk,rhoko),J),nrow=J,ncol=3,byrow=TRUE),
#                        sigK=rep(sig.k,J),
#                        muk=rep(0,J),
#                        var0=array(rep(cov(sdf[sdf$t==1,c("omega","capital")]),J),dim=c(J,2,2)),
#                        SigmaEps=Sigma
# )
#
# jitter.values <- function(v,maxpercent) {
#   vout <- v
#   for(n in names(v)) {
#     vout[[n]] <- sapply(v[[n]], function(x) {
#       x*(1 + maxpercent*runif(1,min=-1,max=1))
#     })
#     dim(vout[[n]]) <- dim(v[[n]])
#   }
#   vout$prob <- vout$prob/sum(vout$prob)
#   return(vout)
# }
#
# for(j in 1:J) {
#   initial.values$var0[j,,] <- cov(sdf[sdf$t==1,c("omega","capital")])
# }
#
# stan.data <- list(NT=N*T, id=sdf$id, time=sdf$t, NTmiss=1,
#                   nmix=J,N=N,T=T,
#                   m=sdf$material, l=sdf$labor, k=sdf$capital,
#                   y=sdf$output)
# tic <- proc.time()
# opt <- optimizing(stan.mle, data=stan.data, init=jitter.values(initial.values,0.5),
#                   algorithm="BFGS",verbose=TRUE,as_vector=FALSE,hessian=TRUE)
# toc <- proc.time()
# toc-tic
#
# if (FALSE) {
#   fit.20 <- sampling(stan.mle, data=stan.data,
#                      init=c(list(initial.values),
#                             replicate(19,
#                                       jitter.values(initial.values,0.25), simplify=FALSE)),
#                      iter=700, chains=20, cores=20, algorithm="NUTS", warmup=200,verbose=TRUE)
#
#   parm <- do.call(c,initial.values)
#   est <- do.call(c,opt$par)
#   parm <- parm[names(est)]
#   se <- diag(-solve(opt$hessian))
#   se <- c(se[1:21],se[21],se[22:25],se[24:39],se[38:43])
#   mcmc <- summary(fit.20)$summary[1:length(est),c("mean","sd")]
#   mcmc <- mcmc[c(1:23,27,25,29,24,28,26,30,31:39,43,41,45,40,44,42,46,47:48),]
#   tab <- cbind(parm,est,se,mcmc )
#
#   colnames(tab) <- c("True Value", "MLE", "SE(MLE)",
#                      "MCMC Posterior Mean", "MCMC Posterior SD")
#   rownames(tab)[11:20] <- as.vector(outer(1:J,1:T,function(x,y)
#     sprintf("b0,j=%d,t=%d",x,y)))
#   rownames(tab)[23:30] <-
#     as.vector(outer(1:J,as.vector(outer(c("eps","zeta"),c("eps","zeta"),function(x,y)
#       paste(x,y,sep=","))), function(x,y) sprintf("Sigma,j=%d,%s",x,y)))
#
#   save(tab, fit.20, file="simulateJoint-results.Rdata")
# }
#
#
# ## Make the likelihood callable from R
# mle.aux <- sampling(stan.mle, data=stan.data,
#                     chains=0)
# loglikelihood <- function(parm) {
#   if (is.list(parm)) {
#     x <- unconstrain_pars(mle.aux, parm)
#   } else {
#     x <- parm
#   }
#   ll <- log_prob(mle.aux,x)
# }
# score <- function(parm) {
#   if (is.list(parm)) {
#     x <- unconstrain_pars(mle.aux, parm)
#   } else {
#     x <- parm
#   }
#   ll <- grad_log_prob(mle.aux,x)
# }
#
# ## optimize using nlopt instead
# x.true <- unconstrain_pars(mle.aux,initial.values)
#
# x0 <- sapply(x.true,function(x) runif(1,x-2,x+2))
# nl.out <- nloptr(x0,function(x) -loglikelihood(x),
#                  function(x) -score(x),
#                  lb=x.true-10, ub=x.true+10,
#                  opts=list(print_level=3, algorithm="NLOPT_LD_SLSQP", maxeval=10000))
# nl.est <- constrain_pars(mle.aux, nl.out$solution)
