---
output:
  pdf_document: default
  html_document:
    self_contained: no
---
Original version produced by Paul Schrimpf; replicated with an R package by Chiyoung Ahn (github.com/chiyahn). 

# Comparing simplified estimator and GMM

This section compares the results of our two-step GMM estimator and a
two-step regression estimator. For simplicity we will focus on single
type estimates in this section.

## Step 1

The first step of each estimator is the same.  Let $x$ denote the log
inputs to the production function, and let $m$ be log materials. Let
$f$ be the log production function. We will assume that $f$ is
Cobb-Douglas. The first step is based on the share equation,
$$ s_{it}
= \log \alpha_m + \log E[e^\epsilon] -
\epsilon_{it}. $$
Using the fact that $\epsilon = -s + E[s]$, we can explicitly solve
for $\alpha_m$,
$$ \alpha_m = 1/E[e^{-s}] $$
Therefore we use the estimate
$\hat{\alpha}_m = 1/\mathbb{E}_n[e^{-s}]$.

## Moment estimator step 2
The second step identifies the remaining parameters are estimating
using the observed output and the assumption that productivity is
Markovian. Let
$$\hat{\epsilon}_{it} = -s_{it} + \mathbb{E}_n[s]$$
Given some parameters, $\theta=(\alpha_k,\alpha_l)$, let
$$ \hat{\omega}_{it}(\theta) = y - \alpha_m m - \alpha_k k - \alpha_l l -
\hat{\epsilon}_{it}, $$
and 
$$ \hat{\eta}_{it}(\theta) = \hat{\omega}_{it} - h(\hat{\omega}_{it-1}), $$
where $h$ is polynomial of some specified degree with coefficients
esitmated by least squares. The second step moments conditions are 
$$ E[\hat{\eta}_{it}(\theta) z_{it} ] = 0. $$

## Regression estimator step 2
The regression estimator uses the fact that conditioning on $x_{it-1}$
contains the information of conditioning on
$\omega_{it-1}$. Therefore, we can estimate
$$ y_{it} - \hat{\beta}_m m_{it} - \hat{\epsilon}_{it} = \beta_l
l_{it} - \beta_k k_{it} + h(x_{it-1}) + \eta_{it} $$
by least squares where $h$ is a polynomial of some specified degree. 

Note that since $\epsilon$ is assumed to be uncorrelated with $k$ and
$l$, it is not essential to subtract $\hat{\epsilon}$ from $y$ for
this estimator. We will see that not subtracting $\hat{\epsilon}$
substantially affects the results.
```{r load data, include=TRUE}
# load data
library(knitr)
library(mixprodtest)
# DATA_NAME = "simdata.csv"
DATA_NAME = "data_production_function_missing2zero.csv"
ind12 <- LoadIndustry(name = DATA_NAME,
                      industry.code = 12)
summary(ind12)
```
<!-- ```{r setup, results='asis'} -->
<!-- # set up webgl -->
<!-- install.lib(rgl) -->
<!-- knit_hooks$set(webgl = hook_webgl) -->
<!-- cat('<script type="text/javascript">',  -->
<!--     readLines(system.file('WebGL','CanvasMatrix.js', package = 'rgl')), -->
<!--     '</script>', sep = '\n') -->
<!-- ``` -->

## Moment estimator results
```{r gmm, include=FALSE, cache=TRUE, autodep=TRUE}
gnr.gmm <- EstimateProdModel(panel.df = ind12, J = 1, t = "year",
                             model = "cobb.douglas", 
                             estimation.method = "classification",
                             classification.method = "stan.em", 
                             second.stage.method = "GMM")
```
```{r gmm-results, include=TRUE}
#kable(t(ModelToSummaryTable(gnr.gmm)),digits=3)
eps <- gnr.gmm$estimates[[1]]$epsilon(gnr.gmm$estimates[[1]]$estimates)
omega <- gnr.gmm$estimates[[1]]$omega(gnr.gmm$estimates[[1]]$estimates)
var(eps)
var(omega)
#correlation(eps,panel.lag(eps,ind12$id,ind12$t))
```

## Regression estimator results

The results below subtract $\epsilon$ from the $y$ in the second step.
```{r lm-results,include=TRUE}
gnr.reg <- EstimateProdModel(panel.df = ind12, J = 1, t = "year",
                             model = "cobb.douglas", 
                             estimation.method = "classification",
                             classification.method = "stan.em", 
                             second.stage.method = "GMM",
                             second.stage.include.eps = FALSE) # default value is TRUE
omega <- gnr.gmm$estimates[[1]]$omega(gnr.reg$estimates[[1]]$estimates)
print(var(omega))
#kable(t(ModelToSummaryTable(gnr.reg)),digits=3)
```
These estimates are very strange with a negative coefficient on
capital. The variance of $\omega$ is about twice as large as in the
GMM estimates. The estimate of $\epsilon$ comes solely from the first
step and is unchanged.

The next results do not subtract $\epsilon$ from $y$. 
```{r lm-results-e,include=TRUE}
gnr.reg <- EstimateProdModel(panel.df = ind12, J = 1, t = "year",
                             model = "cobb.douglas", 
                             estimation.method = "classification",
                             classification.method = "stan.em", 
                             second.stage.method = "GMM",
                             second.stage.include.eps = TRUE) # default value is TRUE
```
```{r, results='asis'}
#kable(t(ModelToSummaryTable(gnr.reg)),digits=3)
```
```{r, include=TRUE}
omega <- gnr.gmm$estimates[[1]]$omega(gnr.reg$estimates[[1]]$estimates)
var(omega)
```
These results are more reasonable, but still noticeably different than
the GMM estimates. The coefficients on capital and labor are both
substantially larger.


## With 3 types
```{r classify, include=FALSE, cache=TRUE, autodep=TRUE}
J <- 3
```
Here we repeat the above, but with 3 types instead of
one. Classification is performed assuming normality.

# ```{r gmm3, include=FALSE, cache=TRUE, autodep=TRUE}
# gmm3 <- EstimateProdModel(panel.df = ind12, J = J, t = "year",
                             # model = "cobb.douglas", 
                             # estimation.method = "classification",
                             # classification.method = "normal.mix.flexmix", 
                             # second.stage.method = "GMM")
<!-- ``` -->
# ```{r lm3, include=FALSE, cache=TRUE, autodep=TRUE}
<!-- lm3 <- EstimateProdModel(panel.df = ind12, J = J, t = "year", -->
<!--                          model = "cobb.douglas",  -->
<!--                          estimation.method = "classification", -->
<!--                          classification.method = "normal.mix.flexmix",  -->
<!--                          second.stage.method = "LM", -->
<!--                          second.stage.include.eps = FALSE) # default value is TRUE -->
<!-- lm3.e <- EstimateProdModel(panel.df = ind12, J = J, t = "year", -->
<!--                          model = "cobb.douglas",  -->
<!--                          estimation.method = "classification", -->
<!--                          classification.method = "normal.mix.flexmix",  -->
<!--                          second.stage.method = "LM", -->
<!--                          second.stage.include.eps = TRUE) # default value is TRUE -->
<!-- ``` -->
### GMM
# ```{r, echo=FALSE}
<!-- kable(ModelToSummaryTable(gmm3), digits=3) -->
<!-- ``` -->
### Regression : without $\epsilon$
# ```{r, echo=FALSE}
# kable(ModelToSummaryTable(lm3), digits=3)
<!-- ``` -->
### Regression : with $\epsilon$
# ```{r, echo=FALSE}
# kable(ModelToSummaryTable(lm3.e), digits=3)
<!-- ``` -->

The results are similar to the single type case. When subtracting
$\epsilon$, the regression estimator gives negative estimes of
$\alpha_k$. The results look more reasonable when not subtracting
$\epsilon$, but the problem of negative coefficients remains for
type 2. Also, the estimates are very different than the GMM estimates
for all three types.

<!-- ```{r} -->
<!-- eta <- gnr.gmm$eta(gnr.gmm$estimates) -->
<!-- ``` -->

# Simulations
This section looks at how the estimators perform on simulated
data. The DGP is as follows. $\omega$ follows an AR(1) process:
$$
\omega_{it} = \rho \times \omega_{it-1} + \eta_{it}
$$
with $\eta_{it} \sim N$. Capital and labor follow a VAR(1)
process conditional on $\omega$. The coefficients are set equal to the
optimal choice of capital (labor) assuming labor (capital) and
$\omega$ stay at their current values and materials is chosen
optimally. This this isn't really what a profit maximizing firm would
do, but since our estimate only makes assumptions about the timing of
the choice of capital, it fits our model. The number of firms is 244
and time periods is 29 as in the observed data. The parameters are set
equal to the GMM estimates.
```{r simulate, include=TRUE, autodep=TRUE, cache=TRUE}
source("simulateProduction.R")
eps.new <- gnr.gmm$estimates[[1]]$epsilon(gnr.gmm$estimates[[1]]$estimates)
omega.new <- gnr.gmm$estimates[[1]]$omega(gnr.gmm$estimates[[1]]$estimates)
sd.eps.new <- sd(eps)
pf <- cobb.douglas(gnr.gmm$estimates[[1]]$estimates[2:3],gnr.gmm$estimates[[1]]$estimates[1],
                   E=exp(0.5*sd.eps.new^2))
rho <- lm(omega ~ panel.lag(omega,ind12$id,ind12$t))$coef
sd.eta <- sd(residuals(lm(omega ~ panel.lag(omega,ind12$id,ind12$t))))
sim.df <- simulateData(pf$production, pf$materials, pf$capital,
                       function(p) { return(rho[1] + rho[2]*p +
                                            rnorm(length(p),sd=sd.eta)) },
                       function(n) { return(rnorm(n,sd=sd.eps.new)) },
                       N = 2*length(unique(ind12$id)),
                       T = 2*length(unique(ind12$t)),
                       initial.productivity=function(n) {
                         sample(omega,size=n,replace=TRUE) } 
                       ,initial.capital=function(n) {
                         index <- 
                           sample.int(nrow(ind12),size=n,replace=TRUE)
                         as.matrix(ind12[index,c("k_it","l_it")])}
                       )
sim.df$share <- sim.df$materials - sim.df$output
summary(sim.df)
summary(lm(output ~ materials + capital + labor, data=sim.df))
```
```{r sim-est, include=FALSE, autodep=TRUE, cache=TRUE}
sim.df$k.lag <- panel.lag(sim.df$capital  , sim.df$id, sim.df$t)
sim.df$l.lag <- panel.lag(sim.df$labor    , sim.df$id, sim.df$t)
sim.df$m.lag <- panel.lag(sim.df$materials, sim.df$id, sim.df$t)
# gmm.sim <- EstimateProdModel(panel.df = sim.df, J = 1, t = "t",
#                              m = "materials", k = "capital", l = "labor", y = "output", lnmY = "share",
#                              model = "cobb.douglas", 
#                              second.stage.instrument.fmla = y_it ~ k_it + l_it, # default for cobb-doug
#                             estimation.method = "classification",
#                             classification.method = "normal.mix.flexmix", 
#                             second.stage.method = "GMM",
#                             initial.values = gnr.gmm$estimates)
```
```{r sim-est-lag, include=FALSE, autodep=TRUE, cache=TRUE}
if (0) {
gmm.sim.lag <- EstimateProdModel(panel.df = sim.df, J = 1, t = "t",
                             m = "materials", k = "capital", l = "labor", y = "output", lnmY = "share",
                             model = "cobb.douglas", 
                             second.stage.instrument.fmla = y_it ~ k_it + l_it + k.lag + l.lag, 
                             estimation.method = "classification",
                             classification.method = "normal.mix.flexmix", 
                             second.stage.method = "GMM",
                             initial.values = gnr.gmm$estimates)
gmm.sim.opt <- EstimateProdModel(panel.df = sim.df, J = 1, t = "t",
                             m = "materials", k = "capital", l = "labor", y = "output", lnmY = "share",
                             model = "cobb.douglas", 
                             second.stage.instrument.fmla = y_it ~ k_it + l_it + k.lag + l.lag + m.lag, 
                             estimation.method = "classification",
                             classification.method = "normal.mix.flexmix", 
                             second.stage.method = "GMM",
                             initial.values = gnr.gmm$estimates)

reg.sim <- EstimateProdModel(panel.df = sim.df, J = 1, t = "t",
                             m = "materials", k = "capital", l = "labor", y = "output", lnmY = "share",
                             model = "cobb.douglas", 
                             estimation.method = "classification",
                             classification.method = "normal.mix.flexmix", 
                             second.stage.method = "LM",
                             second.stage.include.eps = TRUE) # default value is TRUE

reg.noe.sim <- EstimateProdModel(panel.df = sim.df, J = 1, t = "t",
                             m = "materials", k = "capital", l = "labor", y = "output", lnmY = "share",
                             model = "cobb.douglas", 
                             estimation.method = "classification",
                             classification.method = "normal.mix.flexmix", 
                             second.stage.method = "LM",
                             second.stage.include.eps = TRUE) # default value is TRUE
}
```

Estimates based on the simulated data are shown below. The first
column shows the GMM estimates using $k_{it}$ and $l_{it}$ as
instruments. This is the specification that I have been using in all
the empirical results. The second column also $k_{it-1}$ and
$l_{it-1}$ as instruments. The last two columns show the regression
based estimator with and without $\epsilon$. Both regression
estimators produce estimates close to the true parameters.

The difference between the three GMM columns is striking. The estimates
are very close to the true values when lagged capital and labor are
included as instruments, but quite far off when the lags are not
included. This does not appear to be an optimization problem. Both
estimators were initialized at the true parameters. Also, both
estimators draw additional random initial values from the unit simplex
and arrived at the same estimates each time. Similar results are found
when using a continuously updated optimal weighting matrix (which are
the ones shown), when using a diagonal weighint matrix, and when using
empirical likelihood. 

<!-- # ```{r, echo=FALSE} -->
<!-- tbl <- -->
  <!-- cbind((ModelToSummaryTable(gnr.gmm)),(ModelToSummaryTable(gmm.sim)), -->
        <!-- (ModelToSummaryTable(gmm.sim.lag)), -->
<!--         (ModelToSummaryTable(gmm.sim.opt)),  -->
<!--         (ModelToSummaryTable(reg.sim)),(ModelToSummaryTable(reg.noe.sim))) -->
<!-- colnames(tbl) <- c(" True ", -->
<!--                    "  GMM (current k,l) ", -->
<!--                    " GMM (current + lagged k, l) ", -->
<!--                    "  GMM (optimal instruments)  ", -->
<!--                    "Reg. with $\\epsilon$","Reg. no $\\epsilon$") -->
<!-- kable(tbl,digits=3) -->
<!-- ``` -->

Optimal instruments provide a possible explanation for why including
lagged capital and labor is important.  Our conditional moment
restriction is
$$ E[\hat{\eta}_{it}(\theta) | \mathcal{I}_{it-1} ] = 0 $$
where $\mathcal{I}_{it}$ is the firm's information set at time $t-1$,
which includes $l_{it}$ and $k_{it}$ as well as lagged variables. 
Assuming $\eta$ is homoskedastic and independent, the optimal
instrument is
$$ z^*_{it} =
E[ \frac{\partial \hat{\eta}}{\partial \theta} | \mathcal{I}_{it-1} ] $$

Writing $\eta$ in terms of the model, we have

$$ \eta_{it}(\theta) = (\tilde{y}_{it} - \alpha_k k_{it} - \alpha_l
l_{it}) - \rho_0(\theta) - \rho_1(\theta) (\tilde{y}_{it-1} - \alpha_k
k_{it-1} - \alpha_l l_{it-1})$$
where we are assuming $E[\omega_{t}|\omega_{t-1}]$ is linear, and
where $\rho_0$ and
$\rho_1$ are estimated by OLS.

$$ \frac{\partial \eta}{\partial \alpha_k} = -k_{it} + \rho_1
k_{it-1}  - \frac{\partial \rho_1}{\partial \alpha_k} (\tilde{y}_{it-1} - \alpha_k
k_{it-1} - \alpha_l l_{it-1})$$
where $\frac{\partial \rho_1}{\partial \alpha_k}$ is the same for all
$i$ and $t$. Therefore,  $\frac{\partial \eta}{\partial \alpha_k}$ is
a linear function of $k_{it}$, $k_{it-1}$, $l_{it-1}$ and
$\tilde{y}_{it-1}$. Similarly, $\frac{\partial \eta}{\partial \alpha_k}$ is
a linear function of $l_{it}$, $l_{it-1}$, $k_{it-1}$ and
$\tilde{y}_{it-1}$. This suggests that using $k_{it}$, $l_{it}$
$k_{it-1}$, $l_{it-1}$, and $\tilde{y}_{it-1} = y_{it-1} - \alpha_m
m_{it-1} - \hat{\epsilon}_{it-1}$ as instruments should achieve the
same asymptotic variance as the optimal instrument. Not also that
$$ \tilde{y}_{it-1} = y_{it-1} - \alpha_m
m_{it-1} - (-s_{it-1} + \bar{s}) $$
$$ \tilde{y}_{it-1} = y_{it-1} - \alpha_m
m_{it-1} - (-(m_{it-1} - y_{it-1}) + \bar{s}) $$
$$ \tilde{y}_{it-1} = (1 - \alpha_m)m_{it-1}  -  \bar{s} $$
so it suffices to use $m_{it-1}$ in place of $\tilde{y}_{it-1}$. 
The column labeled optimal instruments uses $k_{it}$, $l_{it}$
$k_{it-1}$, $l_{it-1}$, and $m_{it-1}$ as instruments.


# Using optimal instruments with observed data

<!-- # ```{r obs-opt, cache=TRUE, include=FALSE, autodep=TRUE} -->
<!-- ind12$l.lag <- panel.lag(ind12$l_it, ind12$id,ind12$t) -->
<!-- ind12$k.lag <- panel.lag(ind12$k_it, ind12$id,ind12$t) -->
<!-- ind12$m.lag <- panel.lag(ind12$m_it, ind12$id,ind12$t) -->

<!-- gnr.opt <- EstimateProdModel(panel.df = ind12, J = 1, t = "year", -->
<!--                              model = "cobb.douglas",  -->
<!--                              second.stage.instrument.fmla = y_it ~ (k_it + l_it + k.lag + l.lag + m.lag),  -->
<!--                              estimation.method = "classification", -->
<!--                              classification.method = "normal.mix.flexmix",  -->
<!--                              second.stage.method = "GMM", -->
<!--                              initial.values = gnr.gmm$estimates) -->
<!-- ``` -->
<!-- # ```{r opt-ytilde, cache=TRUE, include=FALSE, autodep=TRUE} -->
<!-- alpham <- 1/mean(exp(-ind12$lnmY_it)) -->
<!-- epshat <- -ind12$lnmY_it + mean(ind12$lnmY_it) -->
<!-- y.tilde <- ind12$y_it - alpham*ind12$m_it - epshat -->
<!-- ind12$y.tilde.lag <- panel.lag(y.tilde,ind12$id,ind12$t) -->
<!-- gnr.ytilde <- EstimateProdModel(panel.df = ind12, J = 1, t = "year", -->
<!--                              model = "cobb.douglas",  -->
<!--                              second.stage.instrument.fmla = y_it ~ (k_it + l_it + k.lag + l.lag + y.tilde.lag), -->
<!--                              estimation.method = "classification", -->
<!--                              classification.method = "normal.mix.flexmix",  -->
<!--                              second.stage.method = "GMM", -->
<!--                              initial.values = gnr.gmm$estimates) -->
<!-- ``` -->
<!-- # ```{r, echo=FALSE} -->
<!-- tbl <- cbind((ModelToSummaryTable(gnr.gmm)), (ModelToSummaryTable(gnr.opt)), -->
<!--              (ModelToSummaryTable(gnr.ytilde))) -->
<!-- colnames(tbl) <- c(" GMM (k,l instruments) ", -->
<!--                    " GMM (optimal instruments) ", "GMM (opt. inst. $\\tilde{y}$)") -->
<!-- kable(tbl, digits=3) -->
<!-- ``` -->
These estimates use a diagonal weighting matrix. The weighting matrix does
not matter with two instruments, but does for the optimal
instruments. With optimal instruments, CUE produces estimated
coefficients of 0. The middle column uses $m_{it-1}$ among the
instruments, while the last column uses $\tilde{y}_{it-1}$. The
results are nearly identical. 
