# Comparison of classification estimates when labor is flexible vs fixed

For now, the main purpose of this file is to demonstrate the new
interface. Note that the new interface does not actually do anything
with the labor.share variable. It just uses labor-materials
instead. As a result, the flexible labor estimates below might not
make much sense. 

```{r setup, cache=TRUE, include=TRUE}
J <- 2 #  number of mixture components

library(mixprodtest)

ind12 <- LoadIndustry(name = "data_production_function_missing2zero.csv",
                      industry.code = 12)
ind12$lnlY_it <- ind12$lc - ind12$y_it

var.names <- list(
    id="id",
    t="t",
    output="y_it",
    materials="m_it",
    labor="l_it",
    capital="k_it",
    material.share="lnmY_it",
    labor.share="lnlY_it")

opt <- mixprodtest.options() ## get a list of default options
opt$init.draws <- 1 ## try 1 randomly chosen initial value
opt$estimation.method <- "classification"
opt$second.stage.method <- "GMM"
opt$time.varying <- FALSE
opt$flex.labor <- FALSE


fixed <- mixprodtest.estimate(data=ind12, J, var.names, options=opt)

opt$flex.labor <- TRUE

flex <- mixprodtest.estimate(data=ind12, J, var.names, options=opt)

opt$estimation.method <- "EM"
em.flex <- mixprodtest.estimate(data=ind12, J, var.names, options=opt)
```    
