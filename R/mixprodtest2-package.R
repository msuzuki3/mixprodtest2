# Part of the mixprodtest package for estimating mixture models of
# production functions
# Copyright (C) 2017 Paul Schrimpf, Chiyoung Ahn, Hiro Kasahara, and
# Michio Suzuki
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 3
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#' Mixture models for production functions
#'
#' @docType package
#' @name mixprodtest2-package
#' @aliases mixprodtest2
#' @useDynLib mixprodtest2, .registration = TRUE
#' @import methods
#' @importFrom rstan optimizing
#' @import Rcpp
#' @import rstantools
#' @importFrom rstan sampling
#'
#' @description
#'
#' The \pkg{mixprodtest} package is
#'
#' @section Methods
#'
#' @section Another section
#'
#' @seealso{
#' \itemize
#'   \item FIXME
#'   \item \code{\link{stanreg-objects}} and \code{\link{stanreg-methods}} for
#'   details on the fitted model objects returned by the modeling functions.
#'   \item The custom \code{\link[=plot.stanreg]{plot}} and
#'   \item \url{url}
#' }
#'
#' @references
#' Stan Development Team (2018). RStan: the R interface to Stan. R package version 2.17.3. http://mc-stan.org
#'
NULL
