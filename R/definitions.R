print.summary.production.model <- function(x, ...)
{
  print.production.model(x)
}

print.production.model <- function(x, ...) {
  posterior <- matrix()
  if (class(x$mix) == "flexmix")
    posterior <- posterior(x$mix) 
  else
    posterior <- x$mix$posterior
  J <- ncol(as.matrix(posterior))
  cat(c("Model type: ", x$model$name, "\n"))
  cat(sprintf("The number of components:  %d \n", J))
  cat(sprintf("log.likelihood at estimate:  %.3f \n", x$loglik))
  cat(c("Estimation method: ", x$estimation.method, "\n"))
  cat(sprintf("Estimates: \n"))
  
  print(ModelToSummaryTable(x))
}
