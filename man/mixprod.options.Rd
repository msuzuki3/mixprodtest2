% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/estimate-model.R
\name{mixprod.options}
\alias{mixprod.options}
\title{Options for mixprod}
\usage{
mixprod.options(options = NULL)
}
\arguments{
\item{options}{A named list sepcifying options. If NULL, the
default options will be returned.

Available options: FIXME}
}
\value{
A list of options for mixprod
}
\description{
Returns a list of options with settings for mixprod
}
